# core
[![Build Status](https://drone.anufrij.de/api/badges/artemanufrij/core/status.svg)](https://drone.anufrij.de/artemanufrij/core)

> Artem Anufrij core project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```